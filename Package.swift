// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GitLabSnippetsPublishPlugin",
	platforms: [
		.macOS(.v10_15),
	],
    products: [
        .library(
            name: "GitLabSnippetsPublishPlugin",
            targets: ["GitLabSnippetsPublishPlugin"]),
    ],
    dependencies: [
		.package(url: "https://github.com/johnsundell/publish.git", from: "0.8.0")
    ],
    targets: [
        .target(
            name: "GitLabSnippetsPublishPlugin",
			dependencies: [
				.product(name: "Publish", package: "publish")
			]),
        .testTarget(
            name: "GitLabSnippetsPublishPluginTests",
            dependencies: ["GitLabSnippetsPublishPlugin"]),
    ]
)
