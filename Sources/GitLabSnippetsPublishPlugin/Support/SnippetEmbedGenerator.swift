import Foundation

#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

private extension URLSessionConfiguration {
	class func GitLabSnippetsSessionConfiguration() -> URLSessionConfiguration {
		let config = URLSessionConfiguration.ephemeral
		config.timeoutIntervalForRequest = 10
		return config
	}
}

final class SnippetEmbedGenerator {
	private enum Error: LocalizedError {
		case timeout(id: String)
		case contentTimeout(filepath: String)
		case unexpectedRepsonse
		case httpErrorStatus(code: Int)
		case noData
		case errorDecoding(id: String, error: Swift.Error)
		case errorDecodingContent(filepath: String)

		var localizedDescription: String {
			switch self {
			case .timeout(let id):
				return "The request to GitLab timed out (snippet id: \(id))"
			case .contentTimeout(let filepath):
				return "The request to GitLab timed out (path: \(filepath))"
			case .unexpectedRepsonse:
				return "Unexpected response from GitLab"
			case .httpErrorStatus(let code):
				return "Unexpected status code from GitLab \(code)"
			case .noData:
				return "Received no data from GitLab"
			case .errorDecoding(let id, let error):
				return "Error decoding data from GitLab. Snippet id \(id), error \(error)"
			case .errorDecodingContent(let filepath):
				return "Error decoding data from GitLab. Filepath \(filepath)"
			}
		}
	}

	private var responseCache = [URL: Snippet]()

	func getSnippetData(id: String, url: URL) -> Result<Snippet, Swift.Error> {
		if let cachedSnippetDetails = self.responseCache[url] {
			return .success(cachedSnippetDetails)
		}

		var result: Result<Snippet, Swift.Error> = .failure(Error.timeout(id: id))
		let sema = DispatchSemaphore(value: 0)

		let session = URLSession(configuration: URLSessionConfiguration.GitLabSnippetsSessionConfiguration())

		let task = session.dataTask(with: url) { data, res, error in
			defer { sema.signal() }

			guard let res = res as? HTTPURLResponse else {
				result = .failure(Error.unexpectedRepsonse)
				return
			}

			guard res.statusCode == 200 else {
				result = .failure(Error.httpErrorStatus(code: res.statusCode))
				return
			}

			guard let data = data else {
				if let error = error {
					result = .failure(error)
				} else {
					result = .failure(Error.noData)
				}
				return
			}

			do {
				let snippetDetails = try JSONDecoder().decode(Snippet.self, from: data)
				self.responseCache[url] = snippetDetails
				result = .success(snippetDetails)
			} catch {
				result = .failure(Error.errorDecoding(id: id, error: error))
			}
		}

		task.resume()

		_ = sema.wait(timeout: .now() + 15)

		return result
	}

	func getSnippetContent(filepath: String, url: URL) -> Result<String, Swift.Error> {
		var result: Result<String, Swift.Error> = .failure(Error.contentTimeout(filepath: filepath))
		let sema = DispatchSemaphore(value: 0)

		let session = URLSession(configuration: URLSessionConfiguration.GitLabSnippetsSessionConfiguration())

		let task = session.dataTask(with: url) { data, res, error in
			defer { sema.signal() }

			guard let res = res as? HTTPURLResponse else {
				result = .failure(Error.unexpectedRepsonse)
				return
			}

			guard res.statusCode == 200 else {
				result = .failure(Error.httpErrorStatus(code: res.statusCode))
				return
			}

			guard let data = data else {
				if let error = error {
					result = .failure(error)
				} else {
					result = .failure(Error.noData)
				}
				return
			}

			guard let snippetContent = String(data: data, encoding: .utf8) else {
				result = .failure(Error.errorDecodingContent(filepath: filepath))
				return
			}

			result = .success(snippetContent)
		}

		task.resume()

		_ = sema.wait(timeout: .now() + 15)

		return result
	}
}
