import Foundation
import Plot

public protocol SnippetRenderer {
	func render(snippetName: String, snippetContent: String) throws -> String
}

public final class DefaultSnippetRenderer: SnippetRenderer {
	public init() { }
	public func render(snippetName: String, snippetContent: String) throws -> String {
		return "<pre><code>" + snippetContent.escapingHTMLEntities() + "</code></pre>"
	}
}
