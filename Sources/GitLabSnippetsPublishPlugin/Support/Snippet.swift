import Foundation
import Publish
import Ink
import AppKit

public struct Snippet: Decodable {
	let title: String
	let description: String
	let author: SnippetAuthor
	let files: [SnippetFile]
}

public struct SnippetAuthor: Decodable {
	private enum CodingKeys: String, CodingKey {
		case username
		case name
		case avatarURLString = "avatar_url"
		case webURLString = "web_url"
	}

	let username: String
	let name: String
	let avatarURLString: String
	let webURLString: String

	var avatarURL: URL? { URL(string: self.avatarURLString) }
	var webURL: URL? { URL(string: self.webURLString) }
}

public struct SnippetFile: Decodable {
	private enum CodingKeys: String, CodingKey {
		case path
		case rawURLString = "raw_url"
	}

	let path: String
	let rawURLString: String

	var rawURL: URL? { URL(string: self.rawURLString) }
}
