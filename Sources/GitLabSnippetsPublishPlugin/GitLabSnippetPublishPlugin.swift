import Publish
import Ink
import Files
import Foundation

public extension Plugin {
	static func gitLabSnippets(renderer: SnippetRenderer = DefaultSnippetRenderer(),
							   caching: Bool = false) -> Self {
		if !caching {
			fputs("⚠️ GitLabSnippets caching is disabled. Enable caching to improve generation speed\n", stdout)
		}

		return Plugin(name: "GitLabSnippets") { context in
			context.markdownParser.addModifier(
				.snippetBlockQuote(using: renderer,
								   context: context,
								   fileCaching: caching)
			)
		}
	}
}

private protocol SnippetKey {
	var snippetId: String { get }
	var optionalSnippetPath: String? { get }
	var url: URL { get }
	var cacheIdentifier: String { get }
}

private struct NonProjectSnippetKey: SnippetKey {
	static private let prefix = "> snippet"

	let snippetId: String
	let optionalSnippetPath: String?

	var url: URL {
		URL(string: "https://gitlab.com/api/v4/snippets/\(self.snippetId)")!
	}

	var cacheIdentifier: String {
		if let snippetPath = optionalSnippetPath {
			return "\(snippetId)-\(snippetPath)"
		}

		return snippetId
	}

	init?(markdown: String) {
		guard markdown.starts(with: Self.prefix) else {
			return nil
		}

		let scanner = Scanner(string: markdown)
		_ = scanner.scanString(Self.prefix)

		guard let snippetId = scanner.scanUpToCharacters(from: .whitespacesAndNewlines) else {
			fatalError("Could not find snippet ID in '\(markdown)'")
		}

		self.snippetId = snippetId
		self.optionalSnippetPath = scanner.scanUpToCharacters(from: .whitespacesAndNewlines)
	}
}

private struct ProjectSnippetKey: SnippetKey {
	static private let prefix = "> projectsnippet"

	private let project: String
	let snippetId: String
	let optionalSnippetPath: String?

	var url: URL {
		let escapedProjectPath = self.project.replacingOccurrences(of: "/", with: "%2f")
		return URL(string: "https://gitlab.com/api/v4/projects/\(escapedProjectPath)/snippets/\(self.snippetId)")!
	}

	var cacheIdentifier: String {
		if let snippetPath = optionalSnippetPath {
			return "\(project.replacingOccurrences(of: "/", with: "-"))-\(snippetId)-\(snippetPath)"
		}

		return "\(project.replacingOccurrences(of: "/", with: "%2f"))-\(snippetId)"
	}

	init?(markdown: String) {
		guard markdown.starts(with: Self.prefix) else {
			return nil
		}

		let scanner = Scanner(string: markdown)
		_ = scanner.scanString(Self.prefix)

		guard let project = scanner.scanUpToCharacters(from: .whitespaces) else {
			fatalError("Could not find project path in '\(markdown)'")
		}

		guard let snippetId = scanner.scanUpToCharacters(from: .whitespacesAndNewlines) else {
			fatalError("Could not find snippet ID in '\(markdown)'")
		}

		self.project = project
		self.snippetId = snippetId
		self.optionalSnippetPath = scanner.scanUpToCharacters(from: .whitespacesAndNewlines)
	}
}

private struct Cache: Codable {
	let snippetFilePath: String
	let snippetContent: String
}

private extension Modifier {
	static func snippetBlockQuote<Site: Website>(using renderer: SnippetRenderer,
												 context: PublishingContext<Site>,
												 fileCaching: Bool) -> Self {
		let generator = SnippetEmbedGenerator()
		var memoryCache = [String: Cache]()

		return Modifier(target: .blockquotes) { html, markdown in
			let markdown = String(markdown)
			guard let snippetKey: SnippetKey = NonProjectSnippetKey(markdown: markdown) ??
					ProjectSnippetKey(markdown: markdown) else {
						return html
					}

			var cacheFile: File? = nil

			if let theCacheFile = try? context.cacheFile(named: snippetKey.cacheIdentifier) {
				if fileCaching {
					// try to get the contents of the file cache
					cacheFile = theCacheFile
					if let oldCache = try? theCacheFile.read().decoded() as Cache {
						return try! renderer.render(snippetName: oldCache.snippetFilePath, snippetContent: oldCache.snippetContent)
					}
				} else {
					// delete the file cache
					try? theCacheFile.delete()
				}
			}

			// always try the in-memory cache
			if let cachedSnippetContent = memoryCache[snippetKey.cacheIdentifier] {
				return try! renderer.render(snippetName: cachedSnippetContent.snippetFilePath, snippetContent: cachedSnippetContent.snippetContent)
			}

			let snippetData: Snippet
			let snippetResult = generator.getSnippetData(id: snippetKey.snippetId, url: snippetKey.url)
			switch snippetResult {
			case .success(let theSnippetData):
				snippetData = theSnippetData
			case .failure(let error):
				fatalError(String(describing: error))
			}

			let file: SnippetFile

			if let snippetPath = snippetKey.optionalSnippetPath {
				let filteredFiles = snippetData.files.filter { file in
					file.path == snippetPath
				}

				if filteredFiles.count > 1 {
					fatalError("Multiple files in snippet with id \(snippetKey.snippetId), path \(snippetPath)")
				}

				guard let firstFile = filteredFiles.first else {
					fatalError("No files in snippet with id \(snippetKey.snippetId), path \(snippetPath)")
				}

				file = firstFile
			} else {
				if snippetData.files.count > 1 {
					fatalError("Multiple files in snippet with id \(snippetKey.snippetId). Try specifying a path to select just one of them.")
				}

				guard let firstFile = snippetData.files.first else {
					fatalError("No files in snippet with id \(snippetKey.snippetId)")
				}

				file = firstFile
			}

			guard let url = file.rawURL else {
				fatalError("The raw URL for snippet \(snippetKey.snippetId)/\(file.path) could not be parsed")
			}

			let snippetContent = try! generator.getSnippetContent(filepath: file.path, url: url).get()

			// put in the in-memory cache
			let newCache = Cache(snippetFilePath: file.path, snippetContent: snippetContent)
			memoryCache[snippetKey.cacheIdentifier] = newCache

			// put in the file cache, if enabled
			if fileCaching,
			   let cacheFile = cacheFile {
				try? cacheFile.write(newCache.encoded())
			}

			return try! renderer.render(snippetName: file.path, snippetContent: snippetContent)
		}
	}
}
