# GitLabSnippetsPublishPlugin

## What is this?

This is a plugin for John Sundell's [Publish][publish-github] static site generator, which fetches code from public GitLab Snippets for inclusion in a generated article.

The plugin is inspired by [GistPublishPlugin][gist-plugin-github] by Thomas Lupo, but has one significant difference: when there are multiple files for a Snippet, you must also specify the filename. In contrast, Thomas' GistPublishPlugin will render _all_ files in a Gist.


## How do I use it?

In `main.swift`:

```
import GitLabSnippetsPublishPlugin

try MyWebsite()
	.publish(using: [
		.installPlugin(.gitLabSnippets())
	])
```

This will use a default `Renderer`. You can make your own renderer by passing it into the `gitLabSnippets` function:

```
.instalPlugin(.gitLabSnippets(MyGitLabSnippetsRenderer())
```


### Including a Snippet

In your `page.md` page, add a blockquote starting with the word `snippet`:

```
> snippet 2226922
```

The parameter is the unique ID of the Snippet, which can be found in its URL. (`https://gitlab.com/-/snippets/2226922` for example)

If the Snippet contains multiple files, the filename _must_ be provided:

```
> snippet 2226922 file.swift
```


### Including a project Snippet

For a project-scoped Snippet, use a prefix `projectsnippet` and specify the project path and the Snippet ID:

```
> projectsnippet mflint/gitlabsnippetspublishplugin 2226923
```

And if the project snippet contains multiple files, you'll need the filename too:

```
> projectsnippet mflint/gitlabsnippetspublishplugin 2226923 file.swift
```
 
## Styling the Snippet code

First, you'll need to make your own _renderer_ class. Here's an example which uses the [Splash plugin][splash-github] to style Swift code:

```
import Splash
import GitLabSnippetsPublishPlugin

class SnippetSplashRenderer: SnippetRenderer {
	func render(snippetName: String, snippetContent: String) throws -> String {
		if snippetName.hasSuffix(".swift") {
			let highlighter = SyntaxHighlighter(format: HTMLOutputFormat())
			return "<pre><code>" + highlighter.highlight(snippetContent) + "</code></pre>"
		}

		return "<pre><code>" + snippetContent.escapingHTMLEntities() + "</code></pre>"
	}
}
```

Then, change your `main.swift` to use this new renderer:

```
.installPlugin(.gitLabSnippets(renderer: SnippetSplashRenderer()))
```

Finally, you'll need some CSS to make it look pretty. John has kindly provided an [example CSS file][example-css].

## Caching snippet content

By default, snippet details are cached in memory - which improves the speed of generating your static site if you use multiple files from a single snippet.

You can also enable file caching, which caches snippet contents from one site-generation to the next - which makes generation _much_ faster:

```
.installPlugin(.gitLabSnippets(renderer: SnippetSplashRenderer(), caching: true)),
```

**Warning:** if you enable file caching, then the plugin _will not_ request snippets from GitLab if the file cache exists. If you change your snippets, you'll need to clear the cache for those changes to be included in the generated site. Either delete the cache manually, or temporarily set `caching: false`:

```no-highlight
rm .publish/Caches/install-plugin-gitlabsnippets/*
```

```
.installPlugin(.gitLabSnippets(renderer: SnippetSplashRenderer(), caching: false)),
```


## Feedback

Merge requests most welcome.


## Author

Matthew Flint, m@tthew.org


## License

_GitLabSnippetsPublishPlugin_ is available under the MIT license. See the LICENSE file for more info.


[publish-github]: https://github.com/johnsundell/publish
[gist-plugin-github]: https://github.com/thomaslupo/GistPublishPlugin
[splash-github]: https://githubplus.com/JohnSundell/Splash
[example-css]: https://github.com/JohnSundell/Splash/blob/master/Examples/sundellsColors.css
